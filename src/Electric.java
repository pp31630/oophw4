package ntu.csie.oop13spring;

public class Electric extends POOSkill{
	private int count = 10000;
	
	public Electric(POOPet pet){
		pet.setMP(pet.getMP() - 30);
	}

    public void act(POOPet pet){
        int hp = pet.getHP();
        if (hp > 0)
            pet.setHP(hp - 50);
    }
		
	public int countdown(){
		count--;
		return count;
	}
	
	public int getCountdown(){
		return count;
	}
}
