package ntu.csie.oop13spring;

public class Bubble extends POOSkill{
	private int count = 200000;
	private int direction;
	
	public Bubble(POOPet pet, int dir){
		pet.setMP(pet.getMP() - 5);
		direction = dir;
	}

    public void act(POOPet pet){
        int hp = pet.getHP();
        if (hp > 0)
            pet.setHP(hp - 5);
    }
	
	public int getDirection(){
		return direction;
	}
		
	public int countdown(){
		count--;
		return count;
	}
}

