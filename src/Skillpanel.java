package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.io.File;

class Skillpanel extends JPanel{
	private Image image;
	private int x, y;

	public Skillpanel(POOSkill s) {
		String filename = null;
		try {
			if(s instanceof Bubble){
				filename = "img/bubble.png";
			}
			else if(s instanceof Electric){
				filename = "img/electric.png";
			}
			image = ImageIO.read(new File(filename));
			this.setLayout(null);
			this.setOpaque(false);
		}
		catch (IOException ex) {
		}
	}

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, x, y, null);
    }
	
	public void setXY(int x, int y){
		this.x = x;
		this.y = y;
	}
}