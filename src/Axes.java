package ntu.csie.oop13spring;

public class Axes extends POOCoordinate{
	public POOSkill skill;

	public boolean equals(POOCoordinate other){
		if(skill instanceof Bubble){
			if(x+25 > other.x+20 && x+25 < other.x+80 && y+25 > other.y+20 && y+25 < other.y+80){
				return true;
			}
			else{
				return false;
			}
		}
		else if(skill instanceof Electric){
			if(x > other.x-150 && x < other.x+50 && y > other.y-100 && y < other.y+50){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}
}