package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.util.Random;
import java.lang.Math;

public class Underwater extends POOArena{
	public static int height = 741;
	public static int width = 1024;
	private JFrame frame;
	private Arenapanel arenapanel;
	private Skillpanel[] skillpanel = new Skillpanel[200];
	private Datapanel[] datapanel = new Datapanel[5];
	private Petpanel[] petpanel;
	private POOPet[] pets;
	private POOCoordinate[] coord;
	private POOAction[] action = new POOAction[200];
	private POOCoordinate[] actioncoord = new POOCoordinate[200];

	public Underwater(){
		frame = new JFrame("POOFight");
		frame.getContentPane().setLayout(null);
		frame.setSize(1024, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		arenapanel = new Arenapanel();
		arenapanel.setBounds(0, 150, 1024, 741);
	}
	
    public boolean fight(){
		int alive = 0, countdown;
		POOCoordinate encoord;
		if(pets == null){
			pets = getAllPets();
			petpanel = new Petpanel[pets.length];
			for(int i = 0; i < pets.length; i++){
				petpanel[i] = new Petpanel(pets[i]);
				petpanel[i].setBounds(0, 0, 1024, 741);
				arenapanel.add(petpanel[i]);
				if(pets[i] instanceof Player)
					frame.addKeyListener(new Keyboard((Player)pets[i]));
			}
			for(int i = 0; i < pets.length; i++){
				datapanel[i] = new Datapanel(pets[i]);
				datapanel[i].setBounds(i*202, 0, 200, 150);
				frame.getContentPane().add(datapanel[i]);
			}
			frame.getContentPane().add(arenapanel);
		}
		if(coord == null){
			coord = new POOCoordinate[pets.length];
			for(int i = 0; i < pets.length; i++){
				coord[i] = new Axes();
				coord[i].x = (new Random()).nextInt(1024);
				coord[i].y = (new Random()).nextInt(741);
			}
		}
		for(int i = 0; i < pets.length; i++){
			if(pets[i] != null && pets[i].getHP() > 0){
				for(int j = 0; j < 200; j++)
					if(action[j] == null){
						action[j] = pets[i].act(this);
						if(action[j] != null){
							actioncoord[j] = new Axes();
							if(action[j].skill instanceof Bubble){
								if(((Bubble)action[j].skill).getDirection() == 1){
									actioncoord[j].x = coord[i].x+50;
									actioncoord[j].y = coord[i].y-25;
								}
								else if(((Bubble)action[j].skill).getDirection() == 2){
									actioncoord[j].x = coord[i].x+50;
									actioncoord[j].y = coord[i].y+125;
								}
								else if(((Bubble)action[j].skill).getDirection() == 3){
									actioncoord[j].x = coord[i].x-25;
									actioncoord[j].y = coord[i].y+25;
								}
								else if(((Bubble)action[j].skill).getDirection() == 4){
									actioncoord[j].x = coord[i].x+100;
									actioncoord[j].y = coord[i].y+25;
								}
							}
							else if(action[j].skill instanceof Electric){
								actioncoord[j].x = coord[i].x-30;
								actioncoord[j].y = coord[i].y+125;
							}
							((Axes)actioncoord[j]).skill = action[j].skill;
							skillpanel[j] = new Skillpanel(action[j].skill);
							skillpanel[j].setBounds(0, 0, 1024, 741);
							arenapanel.add(skillpanel[j]);
						}
						break;
					}
				coord[i] = pets[i].move(this);
			}
		}
		for(int i = 0; i < 200; i++){
			if(action[i] != null){
				if(action[i].skill instanceof Bubble){
					countdown = ((Bubble)action[i].skill).countdown();
				}
				else if(action[i].skill instanceof Electric){
					countdown = ((Electric)action[i].skill).countdown();
				}
				else{
					countdown = 0;
				}
				if(countdown == 0){
					action[i] = null;
					actioncoord[i] = null;
					arenapanel.remove(skillpanel[i]);
					skillpanel[i] = null;
				}
				else if(action[i].skill instanceof Bubble){
					if(action[i].dest == null){
						if(countdown%100 == 0 && ((Bubble)action[i].skill).getDirection() == 1){
							actioncoord[i].y -= ((220000-countdown)/50000+1)*((220000-countdown)/50000+1);
						}
						else if(countdown%100 == 0 && ((Bubble)action[i].skill).getDirection() == 3){
							actioncoord[i].x--;
							actioncoord[i].y -= ((220000-countdown)/50000+1)*((220000-countdown)/50000+1);
						}
						else if(countdown%100 == 0 && ((Bubble)action[i].skill).getDirection() == 4){
							actioncoord[i].x++;
							actioncoord[i].y -= ((220000-countdown)/50000+1)*((220000-countdown)/50000+1);
						}
					}
					else{
						if(countdown%1000 == 0){
							encoord = getPosition(action[i].dest);
							actioncoord[i].x += 5*(encoord.x-actioncoord[i].x)/(Math.abs(encoord.x-actioncoord[i].x)+Math.abs(encoord.y-actioncoord[i].y));
							actioncoord[i].y += 5*(encoord.y-actioncoord[i].y)/(Math.abs(encoord.x-actioncoord[i].x)+Math.abs(encoord.y-actioncoord[i].y));
						}
					}
				}
			}
		}
		for(int i = 0; i < pets.length; i++)
			for(int j = 0; j < 200; j++)
				if(pets[i] != null && pets[i].getHP() > 0 && action[j] != null && actioncoord[j].equals(coord[i])){
					if(action[j].skill instanceof Bubble){
						((Bubble)action[j].skill).act(pets[i]);
						action[j] = null;
						actioncoord[j] = null;
						arenapanel.remove(skillpanel[j]);
						skillpanel[j] = null;
					}
					else if(action[j].skill instanceof Electric){
						if(((Electric)action[j].skill).getCountdown() < 5000){
							((Electric)action[j].skill).act(pets[i]);
							action[j] = null;
							actioncoord[j] = null;
							arenapanel.remove(skillpanel[j]);
							skillpanel[j] = null;
						}
					}
				}
		for(int i = 0; i < pets.length; i++)
			if(pets[i] != null){
				if(pets[i].getHP() > 0){
					alive++;
				}
				else{
					arenapanel.remove(petpanel[i]);
					petpanel[i] = null;
					if(pets[i] instanceof Player){
						alive = 0;
						break;
					}
					pets[i] = null;
				}
			}
		if(alive < 2){
			for(int i = 0; i < 5; i++){
				frame.getContentPane().remove(datapanel[i]);
				datapanel[i] = null;
				Messagepanel messagepanel = null;
				if(alive == 0){
					messagepanel = new Messagepanel("You lose!");
				}
				else if(alive == 1){
					messagepanel = new Messagepanel("You won!");
				}
				messagepanel.setBounds(0, 0, 1008, 150);
				frame.getContentPane().add(messagepanel);
			}
			frame.repaint();
			return false;
		}
		else
			return true;
	}
	
    public void show(){
		for(int i = 0; i < pets.length; i++){
			if(pets[i] != null)
				petpanel[i].setXY(coord[i].x, coord[i].y);
		}
		for(int i = 0; i < 200; i++)
			if(action[i] != null){
				skillpanel[i].setXY(actioncoord[i].x, actioncoord[i].y);
			}
		for(int i = 0; i < pets.length; i++){
			if(pets[i] != null){
				datapanel[i].setData(pets[i].getClass().getName(), pets[i].getHP(), pets[i].getMP(), pets[i].getAGI());
			}
			else{
				datapanel[i].setData("Dead", 0, 0, 0);
			}
		}
		frame.repaint();
	}
	
    public POOCoordinate getPosition(POOPet p){
		for(int i = 0; i < pets.length; i++)
			if(pets[i] != null && p == pets[i])
				return coord[i];
		return null;
	}
	
	public int getPetnum(){
		return pets.length;
	}
}
