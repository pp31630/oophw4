package ntu.csie.oop13spring;

public class Player extends POOPet{
	private static int recoveryrate = 10000;
	private static int moverate = 2000;
	private int movecountdown;
	private int recovercountdown;
	private boolean up, down, left, right;
	private boolean atk_up, atk_down, atk_left, atk_right;
	
	private void recoverMP(){
		if(recovercountdown == 0){
			setMP(getMP() + 1);
			recovercountdown = recoveryrate;
		}
		recovercountdown--;
	}
	
	public Player(){
		setHP(200);
		setMP(100);
		setAGI(10);
		movecountdown = moverate/getAGI();
		recovercountdown = recoveryrate;
	}

    protected POOAction act(POOArena arena){
		if(getMP() < 100)
			recoverMP();
		if(atk_up | atk_down | atk_left | atk_right){
			POOAction action = new POOAction();
			action.dest = null;
			if(atk_up && getMP() >= 5){
				action.skill = new Bubble(this, 1);
				atk_up = false;
				return action;
			}
			else if(atk_down && getMP() >= 30){
				action.skill = new Electric(this);
				atk_down = false;
				return action;
			}
			else if(atk_left && getMP() >= 5){
				action.skill = new Bubble(this, 3);
				atk_left = false;
				return action;
			}
			else if(atk_right && getMP() >= 5){
				action.skill = new Bubble(this, 4);
				atk_right = false;
				return action;
			}
			else{
				atk_up = false;
				atk_down = false;
				atk_left = false;
				atk_right = false;
				return null;
			}
		}
		return null;
	}

    protected POOCoordinate move(POOArena arena){
		Underwater underwater = (Underwater)arena;
		POOCoordinate coord = arena.getPosition(this);
		if(movecountdown == 0){
			if(up){
				coord.y--;
			}
			if(down){
				coord.y++;
			}			if(left){
				coord.x--;
			}			if(right){
				coord.x++;
			}
			if(coord.x < 0){
				coord.x = 0;
			}
			if(coord.y < 0){
				coord.y = 0;
			}
			if(coord.x > underwater.width-100){
				coord.x = underwater.width-100;
			}
			if(coord.y > underwater.height-100){
				coord.y = underwater.height-100;
			}
			movecountdown = moverate/getAGI();
		}
		movecountdown--;		return coord;
	}
	
	public void setUP(boolean up){
		this.up = up;
	}
	
	public void setDOWN(boolean down){
		this.down = down;
	}
	
	public void setLEFT(boolean left){
		this.left = left;
	}
	
	public void setRIGHT(boolean right){
		this.right = right;
	}
	
	public void setATKUP(){
		this.atk_up = true;
	}
	
	public void setATKDOWN(){
		this.atk_down = true;
	}
	
	public void setATKLEFT(){
		this.atk_left = true;
	}
	
	public void setATKRIGHT(){
		this.atk_right = true;
	}
}
