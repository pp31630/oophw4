#!/bin/sh

ARENA_CLASS=ntu.csie.oop13spring.Underwater
PET1_CLASS=ntu.csie.oop13spring.Player
PET2_CLASS=ntu.csie.oop13spring.Jellyfish
PET3_CLASS=ntu.csie.oop13spring.Jellyfish
PET4_CLASS=ntu.csie.oop13spring.Jellyfish
PET5_CLASS=ntu.csie.oop13spring.Jellyfish

java -jar hw4.jar $ARENA_CLASS $PET1_CLASS $PET2_CLASS $PET3_CLASS $PET4_CLASS $PET5_CLASS
