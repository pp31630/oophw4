package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.io.File;

class Arenapanel extends JPanel{
	private Image image;

	public Arenapanel() {
		try {                
			image = ImageIO.read(new File("img/underwater.png"));
			this.setLayout(null);
		}
		catch (IOException ex) {
		}
	}

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);     
    }
}