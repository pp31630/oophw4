package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;

class Messagepanel extends JPanel{
	private JLabel message;

	public Messagepanel(String msg) {
		this.setLayout(null);
		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.blue, 2), BorderFactory.createEtchedBorder()));
		this.add(message = new JLabel(msg));
		message.setBounds(400, 0, 1200, 160);
		message.setFont(new Font("Bold", 0, 50));
		message.setForeground(Color.red);
	}

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}