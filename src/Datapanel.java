package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.io.File;

class Datapanel extends JPanel{
	private JLabel NAME, HP, MP, AGI;
	private Image image;
	private static int jellycount = 1;

	public Datapanel(POOPet p) {
		this.setLayout(null);
		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.blue, 2), BorderFactory.createEtchedBorder()));
		this.add(NAME = new JLabel());
		this.add(HP = new JLabel());
		this.add(MP = new JLabel());
		this.add(AGI = new JLabel());
		NAME.setBounds(10, 5, 200, 50);
		HP.setBounds(10, 35, 200, 50);
		MP.setBounds(10, 65, 200, 50);
		AGI.setBounds(10, 95, 200, 50);
		NAME.setFont(new Font("Bold", 0, 20));
		HP.setFont(new Font("Bold", 0, 20));
		MP.setFont(new Font("Bold", 0, 20));
		AGI.setFont(new Font("Bold", 0, 20));
		String filename = null;
		try {
			if(p instanceof Player){
				filename = "img/player.png";
			}
			else if(p instanceof Jellyfish){
				filename = "img/jellyfish" + jellycount + ".png";
				jellycount++;
			}
			image = ImageIO.read(new File(filename));
		}
		catch (IOException ex) {
		}
	}

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
		g.drawImage(image, 90, 40, null);
    }
	
	public void setData(String name, int _HP, int _MP, int _AGI){
		if(name.equals("Dead")){
			NAME.setText("Dead");
		}
		else{
			String str[] = name.split("\\.");
			NAME.setText("NAME: " + str[3]);
		}
		HP.setText("HP: " + _HP);
		MP.setText("MP: " + _MP);
		AGI.setText("AGI: " + _AGI);
	}
}