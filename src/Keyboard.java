package ntu.csie.oop13spring;

import java.awt.event.*;

public class Keyboard extends KeyAdapter{
	private Player p;

	public Keyboard(Player p){
		this.p = p;
	}

	public void keyTyped(KeyEvent e){
    }
     
    public void keyPressed(KeyEvent e){
		if(e.getKeyCode() == KeyEvent.VK_UP){
			p.setUP(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN){
			p.setDOWN(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT){
			p.setLEFT(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			p.setRIGHT(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_W){
			p.setATKUP();
		}
		else if(e.getKeyCode() == KeyEvent.VK_S){
			p.setATKDOWN();
		}
		else if(e.getKeyCode() == KeyEvent.VK_A){
			p.setATKLEFT();
		}
		else if(e.getKeyCode() == KeyEvent.VK_D){
			p.setATKRIGHT();
		}
    }
     
    public void keyReleased(KeyEvent e){
		if(e.getKeyCode() == KeyEvent.VK_UP){
			p.setUP(false);
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN){
			p.setDOWN(false);
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT){
			p.setLEFT(false);
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			p.setRIGHT(false);
		}
    }
}