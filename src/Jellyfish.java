package ntu.csie.oop13spring;

import java.util.Random;
import java.lang.Math;

public class Jellyfish extends POOPet{
	private static int recoveryrate = 10000;
	private static int moverate = 2000;
	private int movecountdown;
	private int recovercountdown;
	
	private void recoverMP(){
		if(recovercountdown == 0){
			setMP(getMP() + 1);
			recovercountdown = recoveryrate;
		}
		recovercountdown--;
	}

	public Jellyfish(){
		setHP(100);
		setMP(100);
		setAGI(5);
		movecountdown = moverate/getAGI();
		recovercountdown = recoveryrate;
	}

    protected POOAction act(POOArena arena){
		POOPet[] pets = arena.getAllPets(); 
		int choice = (new Random()).nextInt(30000);
		POOAction action = new POOAction();
		
		if(getMP() < 100)
			recoverMP();
		for(int i = 0; i < pets.length; i++){
			if(pets[i].getHP() > 0 && pets[i] instanceof Player){
				action.dest = pets[i];
			}
		}
		if(choice == 0 && getMP() >= 5){
			POOCoordinate encoord = arena.getPosition(action.dest);
			POOCoordinate mycoord = arena.getPosition(this);
			double arctan = Math.atan((double)(encoord.y-mycoord.y)/(encoord.x-mycoord.x));
			if((arctan >= 0.785 || arctan < -0.785) && encoord.y-mycoord.y < 0)
				action.skill = new Bubble(this, 1);
			else if((arctan >= 0.785 || arctan < -0.785) && encoord.y-mycoord.y >= 0)
				action.skill = new Bubble(this, 2);
			else if((arctan >= -0.785 || arctan < 0.785) && encoord.x-mycoord.x < 0)
				action.skill = new Bubble(this, 3);
			else if((arctan >= -0.785 || arctan < 0.785) && encoord.x-mycoord.x >= 0)
				action.skill = new Bubble(this, 4);
			return action;
		}
		return null;
	}

    protected POOCoordinate move(POOArena arena){
		Underwater underwater = (Underwater)arena;
		POOCoordinate coord = arena.getPosition(this);
		
		if(movecountdown == 0){
			int choice = (new Random()).nextInt(4);
			switch(choice){
				case 0:
					coord.x++;
					break;
				case 1:
					coord.x--;
					break;
				case 2:
					coord.y++;
					break;
				case 3:
					coord.y--;
					break;
			}
			
			if(coord.x < 0){
				coord.x = 0;
			}
			if(coord.y < 0){
				coord.y = 0;
			}
			if(coord.x > underwater.width-100){
				coord.x = underwater.width-100;
			}
			if(coord.y > underwater.height-100){
				coord.y = underwater.height-100;
			}
			movecountdown = moverate/getAGI();
		}
		movecountdown--;
		return coord;
	}
}