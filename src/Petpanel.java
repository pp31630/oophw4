package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.io.File;

class Petpanel extends JPanel{
	private Image image;
	private int x, y;
	private static int jellycount = 1;

	public Petpanel(POOPet p) {
		String filename = null;
		try {
			if(p instanceof Player){
				filename = "img/player.png";
			}
			else if(p instanceof Jellyfish){
				filename = "img/jellyfish" + jellycount + ".png";
				jellycount++;
			}
			image = ImageIO.read(new File(filename));
			this.setLayout(null);
			this.setOpaque(false);
		}
		catch (IOException ex) {
		}
	}

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
		g.drawImage(image, x, y, null);
    }
	
	public void setXY(int x, int y){
		this.x = x;
		this.y = y;
	}
}